<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class User extends Authenticatable
{

    protected $fillable = [
        'Username', 'password', 'email', 'phone', 'fileToUpload',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    
    
/* author-asgarali
   email-asgarali14292@gmail.com
*/
    
    
    
    public function addUser()
    {
//die('dvsd');
        if (func_num_args() > 0) {

            $userData = func_get_arg(0);
            try {
                $result = DB::table('users')

                    ->insert($userData);
//                dd($result);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            echo 'Value not passed';
        }


    }

    public function getUserById($userId)
    {
        $result = User::whereId($userId)->first();
        return $result;
    }

    public function getUserData()
    {

        $result = DB::table('users')
            ->get();
        return $result;

    }

    public function UpdateUserDetails()
    {

        if (func_num_args() > 0) {

            $userData = func_get_arg(0);
            $userid = func_get_arg(1);
            try {
                $result = DB::table('users')
                    ->where('id', $userid)
                    ->update($userData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            echo 'Not Passed';
        }

//    public function getUserData(){
//
//        $result = DB::table('users')
//            ->get();
//        return $result;
    }
}