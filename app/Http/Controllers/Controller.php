<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use App\User;
use Session;
use Illuminate\Support\Facades\Auth;

use View;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
        public function register(Request $request)
{


    return view("/users/register");

}
    public function login(Request $request)
    {

        if (Session::has('user')) {
            return redirect('/show');
        }
        if ($request->isMethod('post')) {
            $Username = $request->input('Username');
            $password = $request->input('password');
            if (Auth::attempt(['Username' => $Username, 'password' => $password])){
//                  die('hbgfvksdhvgka');
                $objModelUsers = new User();
                $userDetails = $objModelUsers->getUserById(Auth::id());
                $sessionName = 'user';
//                    Session::put();
                Session::put($sessionName, $userDetails['original']);
                
                return redirect()->intended('/show');

//                echo '<pre>';
//            print_r($userDetails);
//            die;



//                if ($userData->role == 0) {
//                    $sessionName = 'ror_user';
//                    Session::put($sessionName, $userData['original']);
//                    return redirect()->intended('/');
//                } else {
//                    return Redirect::back()->with(['status' => 'error', 'msg' => 'invalid creds.']);
//                }
            } else {
                die('invalid data');
//                dd(Auth::attempt(['email' => $email, 'password' => $password]));
                return Redirect::back()->with(['status' => 'error', 'msg' => 'invalid creds.']);
            }
        }
        return view("/users/login");}

public  function addUser(Request $request)
{

    if ($request->isMethod('post')) {


        $userData['Username'] = $request->input('Username');
        $userData['password'] = Hash::make($request->input('password'));
        $userData['email'] = $request->input('email');
        $userData['phone'] = $request->input('phone');




        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        $userData['image']=$target_file;
//dd($userData);

        $objInsertData = new User();
        $result = $objInsertData->addUser($userData);
        if ($result) {
            die('data inserted');

        }

//        $objInsertData = new User();
//        $result = $objInsertData->addUser($userData);
//        if ($result) {
//            die('data inserted');
//        }
//             echo '<pre>';
//          print_r($userData);
////           die;
////          die('cxvxcv');


    }
    return view("/users/register");
}
public function show(){
    $objData = new User();
    $result = $objData->getUserData();
    return view("/users/show", ['result' => $result]);
}
    public function logout(){
        Session::flush('user');
        return redirect('/login');
    }

    public function destroy($id)
    {
        user::find($id)->delete();
        return redirect('/show');
    }



    public function edit($id)
    {

        $userDetails = User::find($id);

        return view('/users/edit',compact('userDetails'));

    }

    public function update(Request $request)
    {
        $userid =$request->input('id');
        $userData['Username'] = $request->input('updateusername123');
        $userData['email'] = $request->input('updateemail');
        $userData['phone'] = $request->input('updatephone');
//        echo '<pre>';
//      print_r($userData);
//        die;


        $objModelUsers = new User();
        $userDetails = $objModelUsers->UpdateUserDetails($userData,$userid);

        return redirect('/show');

    }
}

