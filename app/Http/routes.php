<?php

/*
|------------------------------------------------------------------------—
| Application Routes
|------------------------------------------------------------------------—
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/login', function () {
//   return view('login');
//});

//Route::resource('register','Controller@registers');


Route::resource('/register','Controller@register');

Route::resource('/add-user','Controller@addUser');

Route::resource('/login','Controller@login');
Route::resource('/logout','Controller@logout');

Route::resource('/users','userController');

Route::resource('/show','Controller@show');
Route::resource('/destroy','Controller@destroy');

Route::resource('/edit','Controller@edit');
Route::resource('/update','Controller@update');




