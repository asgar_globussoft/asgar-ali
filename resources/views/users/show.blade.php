<html>
<body>

<h1>MY DATA</h1>

       <div class="row">
           <div class="col-lg-12">
               <div class="panel panel-default">
                   <div class="panel-body">
                       <div class="text-right">

                       </div>
                       <div class="table-responsive">
                           <table class="table table-striped font-14" id="datatable">

                                   <table border="5" style="width:100%">

                               <thead>
                               <tr>

                                   <th>Username</th>
                                   <th>Email</th>
                                   <th>Phone</th>
                                   <th>Image</th>
                                   <th>Action</th>

                               </tr>
                               </thead>
                               <tbody>
                               @foreach  ($result as $userDetails)


                               <tr>

                                   <td> <?php echo $userDetails->Username; ?></td>
                                   <td> <?php echo $userDetails->email; ?></td>
                                   <td><?php echo $userDetails->phone; ?></td>
                                   <td><img src="{{$userDetails->image}}" height="50" width="80" class="img-rounded"></td>

                                   <form method="post" action="/edit">
                                   <td><button><a href="/edit/<?php echo $userDetails->id?>"
                                           class="btn btn-warning">Edit</a></button>
                                   </form>
                                   <form method="post" action="/show">
                                 <button> <a href="/destroy/<?php echo $userDetails->id?>"
                                      class="btn btn-danger">Delete</a></button></td>

                                   </form>

                               </tr>
                               @endforeach
                               </tbody>
                               </table>
                               </table>

                       </div>
                   </div>
               </div>
           </div>

           </div>
       <form method="post" action="/logout">
           <button type="submit">LOG OUT!</button>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
       </form>
</body>
</html>
